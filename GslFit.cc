#include <gsl/gsl_multifit_nlinear.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_histogram.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <valarray>

using Values = std::valarray<double>;

struct Data
{
  const Values& _x;
  const Values& _y;
};

double g(double x, double mu, double sigma)
{
  return 1 / (sqrt(2 * M_PI) * sigma) * exp(-pow((x - mu) / sigma,2) / 2);
}

int chi2(const gsl_vector* p,
	 void*             data,
	 gsl_vector*       f)
{
  Data*         d = static_cast<Data*>(data);
  const Values& x = d->_x;
  const Values& y = d->_y;

  double mu    = gsl_vector_get(p, 0);
  double sigma = gsl_vector_get(p, 1);

  for (size_t i = 0; i < x.size(); i++) {
    double r = y[i] - g(x[i], mu, sigma);
    gsl_vector_set(f, i, r);
  }
  return GSL_SUCCESS;
}

void callback(const size_t iter, void* params,
	      const gsl_multifit_nlinear_workspace* ws)
{
#if 0
  gsl_vector* f     = gsl_multifit_nlinear_residual(ws);
  gsl_vector* p     = gsl_multifit_nlinear_position(ws);
  double      mu    = gsl_vector_get(p, 0);
  double      sigma = gsl_vector_get(p, 1);
  double      chi2  = gsl_blas_dnrm2(f);
  std::cout << iter << ":"
	    << " mu=" << mu
	    << " sigma=" << sigma
	    << " chi2=" << chi2 << std::endl;
  // double rcond;
  /* compute reciprocal condition number of J(x) */
  // gsl_multifit_nlinear_rcond(&rcond, w);
  // std::cout << "Condition(J) " << 1 / rcond << std::endl;
#endif
}

int fit(const char* filename)
{
  gsl_histogram* h = gsl_histogram_alloc(30);
  gsl_histogram_set_ranges_uniform(h, -3, 3);
  std::ifstream in(filename);
  while (!in.eof()) {
    double x;
    in >> x;
    if (in.bad()) break;
    gsl_histogram_increment(h, x);
  }

  size_t nh  = gsl_histogram_bins(h);
  double sum = gsl_histogram_sum(h);
  gsl_histogram_scale(h, 1/sum);
  Values x(nh);
  Values y(nh);
  Values e(nh);
  for (size_t i = 0; i < nh; i++) {
    double x1, x2;
    gsl_histogram_get_range(h, i, &x1, &x2);
    double dx = (x2-x1);
    x[i] = (x2+x1) / 2;
    y[i] = gsl_histogram_get(h, i) / dx;
    e[i] = sqrt(y[i] / sum / dx);
  }
  size_t np = 2;
  Data   d  = {x, y};
  
  gsl_multifit_nlinear_parameters wsp =
    gsl_multifit_nlinear_default_parameters();
  gsl_multifit_nlinear_workspace* ws =
    gsl_multifit_nlinear_alloc(gsl_multifit_nlinear_trust,  &wsp,
			       x.size(), np);

  gsl_multifit_nlinear_fdf fdf;
  fdf.f     = chi2;
  fdf.df     = NULL;
  fdf.fvv    = NULL;
  fdf.n      = x.size();
  fdf.p      = np;
  fdf.params = &d;

  Values          w2      = 1. / pow(e,2);
  w2[e<=0] = 0;
  double          pinit[] = {-.5, 2};
  gsl_vector_view p       = gsl_vector_view_array(pinit,np);
  gsl_vector_view w       = gsl_vector_view_array(&(w2[0]),e.size());

  gsl_multifit_nlinear_winit(&p.vector, &w.vector, &fdf, ws);

  gsl_vector* f      = gsl_multifit_nlinear_residual(ws);
  double      chisq0;
  gsl_blas_ddot(f, f, &chisq0);

  const double ptol = 1e-8;
  const double gtol = 1e-8;
  const double ftol = 0;

  int info   = 0;
  int status = gsl_multifit_nlinear_driver(100, ptol, gtol, ftol,
					   callback, NULL, &info, ws);

  gsl_matrix* c = gsl_matrix_alloc(np,np);
  gsl_matrix* j = gsl_multifit_nlinear_jac(ws);
  gsl_multifit_nlinear_covar(j, 0.0, c);

  double chisq;
  gsl_blas_ddot(f, f, &chisq);

  double nu    = x.size() - np;
  double chi2r = GSL_MAX_DBL(1, sqrt(chisq / nu));
  std::cout << std::fixed
	    << "chi^2/nu: " << std::setprecision(1) << chisq
	    << "/" << int(nu)
	    << "=" << std::setprecision(3) << chisq / nu
	    << " (" << std::setprecision(1)
	    << 100 * gsl_cdf_chisq_Q(chisq,nu) << "%)\n";
  const char* pn[] = {"mu", "sigma"};
  std::cout << std::setprecision(2);
  for (size_t i = 0; i < np; i++) 
    std::cout << std::left
	      << std::setw(6) << pn[i] << ": "
	      << std::right
	      << std::setw(5) << gsl_vector_get(ws->x, i) << " +/- "
	      << std::setw(5) << sqrt(gsl_matrix_get(c, i, i))*chi2r
	      << std::endl;
    
#if 0
  std::cout << "Summary from method "
	    << gsl_multifit_nlinear_name(ws) << "/"
	    << gsl_multifit_nlinear_trs_name(ws) << "\n"
	    << "Number of iterations: " << fdf.nevalf << "\n"
	    << "Jacobian evaluations: " << fdf.nevaldf << "\n"
	    << "Reason for stopping:  small " << (info==1?"step":"gradient")
	    << "\n"
	    << "Initial chi^2:        " << chisq0 << "\n"
	    << "Final chi^2:          " << chisq << "\n";

  std::cout << "chi^2/nu:             " << chisq / nu << "\n"
	    << "Probability:          " << gsl_cdf_chisq_Q(chisq,nu) << "\n";
  for (size_t i = 0; i < np; i++) 
    std::cout << "p" << i+1 << ":                   "
	      << gsl_vector_get(ws->x, i) << " +/- "
	      << sqrt(gsl_matrix_get(c, i, i))*chi2r
	      << std::endl;
  std::cout << "Status:               " << gsl_strerror(status) << std::endl;
#endif 

  return status;
}

int main(int argc, char** argv)
{
  return fit(argv[1]);
}
  
