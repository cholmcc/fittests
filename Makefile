#
#
#
ALL	:= gsl_numpy.out	\
	   gsl_root.out		\
	   gsl_gsl.out		\
	   root_numpy.out	\
	   root_root.out	\
	   root_gsl.out		\
	   scipy_numpy.out	\
	   scipy_root.out	\
	   scipy_gsl.out	
DIFF	:= numpy.diff gsl.diff root.diff

all:	$(DIFF)
#all:	$(ALL)

show:	$(ALL)
	$(foreach g,numpy root gsl,\
		echo "Data: $(g)";\
		$(foreach f,gsl root scipy,\
			echo " Fitter: $(f)"; \
			cat $(f)_$(g).out | sed 's/^/  /'; ))

showr:	$(ALL)
	$(foreach f,gsl root scipy,\
		echo "Fitter $(f)";\
		$(foreach g,numpy root gsl,\
			echo " Data $(g)"; \
			cat $(f)_$(g).out | sed 's/^/  /'; ))


clean:
	rm -f *.dat *.out *.diff GslFit *~ 


.PHONY:	show

## -------------------------------------------------------------------
gsl.dat:
	gsl-randist 11111 1000 gaussian 1 > $@

root.dat:
	root -l -b -q RootGen.C | tail +3 > $@

numpy.dat:
	python3 NumpyGen.py > $@

## -------------------------------------------------------------------
root_%.out:RootFit.C %.dat
	root -l -b -q $<\(\"$*.dat\"\) | tail +3 > $@

gsl_%.out:GslFit %.dat
	./$< $*.dat  > $@

scipy_%.out:ScipyFit.py %.dat
	python3 $< $*.dat  > $@

## -------------------------------------------------------------------
%.diff:	root_%.out scipy_%.out gsl_%.out
	@echo "=== $*"
	-@diff3 $^

# numpy.diff:	root_numpy.out 
## -------------------------------------------------------------------
GslFit:GslFit.cc
	g++ $< -o $@ $(shell gsl-config --cflags --libs)


gsl_numpy.out:GslFit numpy.dat
gsl_root.out: GslFit root.dat
gsl_gsl.out:  GslFit gsl.dat


root_numpy.out:RootFit.C numpy.dat
root_root.out: RootFit.C root.dat
root_gsl.out:  RootFit.C gsl.dat

scipy_numpy.out:ScipyFit.py numpy.dat
scipy_root.out: ScipyFit.py root.dat
scipy_gsl.out:  ScipyFit.py gsl.dat
#
# EOF
#
