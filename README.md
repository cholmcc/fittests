# fittests

Some tests of using different fitters.

## General description

### Data generation 

This project will generate $`N_{\mathrm{samp}}=1000`$ random values
drawn from a normal distribution

```math
P_{\mathrm{norm}}(x;\mu,\sigma) =
\frac{1}{\sqrt{2\pi}\sigma}e^{-\frac{(x-\mu)^2}{2\sigma^2}}\quad,
```
	
with $`\mu=0`$ and $`\sigma=1`$.  

### Histogramming

We histogram these samples into a histogram of $`N_{\mathrm{bin}}=30`$
bins between $`x_{\mathrm{low}}=-3`$ and $`x_{\mathrm{high}}=3`$.  The
bin boundaries for bin $`i`$ are then
	
```math
b_{0}^{\mathrm{low}} = 
x_{\mathrm{low}}+i(x_{\mathrm{high}}-x_{\mathrm{low}})/N_{\mathrm{bin}}
\qquad
b_{i}^{\mathrm{high}} = 
x_{\mathrm{low}}+(i+1)(x_{\mathrm{high}}-x_{\mathrm{low}})/N_{\mathrm{bin}}
\quad,
``` 
	
The bin content in the $`i^{\mathrm{th}}`$ bin is given by 

```math
y_i = \frac{N_i}{N_{\mathrm{samp}}\Delta_i}\quad,
```
	
where $`\Delta_i = b_{i}^{\mathrm{high}}-b_{i}^{\mathrm{low}}`$ is the
bin width, and $`N_i`$ is the number of samples $`s_j`$ for which 

```math
b_{i}^{\mathrm{low}} < s_j < -b_{i}^{\mathrm{high}}
\quad.
```
	
The uncertainty in each bin is then 

```math
\delta_i = \sqrt{\frac{y_i}{N_{\mathrm{samp}}\Delta_i}}\quad.
```
	
### Curve fitting 

Next, programs fit the function 

```math
f(x;\mu,\sigma) =
\frac{1}{\sqrt{2\pi}\sigma}e^{-\frac{(x-\mu)^2}{2\sigma^2}}\quad,
```
	
with free parameters $`\mu`$ and $`\sigma`$ to the data 

```math
\left\{(x_i,y_i,\delta_i)|i=1,\ldots,N_{\mathrm{bins}}\right\}\quad,
```
    
using non-linear fits.  The final $`\chi^2`$, $`\nu`$, and parameter
values and uncertainties are reported, together with the $`\chi^2`$
probability. 

## Concrete implementations 

### Data generation 

- `gsl.dat` is generated by the program `gsl-randist` via the
  [`Makefile`](Makefile)
- `numpy.dat` is generated by the Python script
  [`NumpyGen.py`](NumpyGen.py). The script uses
  `numpy.random.normal`. 
- `root.dat` is generated by the script [`RootGen.C`](RootGen.C) which
  uses `TRandom::Gaus` for the generation 

### Histogrammers and fitters 

- [`GslFit.cc`](GslFit.cc) defines a program that uses _GNU Scientific
  Library_ for histogramming and fitting. 
- [`ScipyFit.py`](Scipyfit.py) is a Python script that uses _NumPy_
  and _SciPy_ for histogramming and curve fitting, respectively. 
- [`RootFit.C`](RootFit.C) uses _ROOT_ for both histogramming and
  fitting (the fitter used is _Minuit_). 
  
## Tests 

The top-level [`Makefile`](Makefile) defines targets to make all
combinations of generators and fitters.  In that way we can evaluate
the performance of each fitter against the other fitters with the same
input data.  To see the differences between the fitters, do 

    make 
    
We can ofcourse clean-up by 

    make clean 
    
(c) 2019 Christian Holm Christensen
