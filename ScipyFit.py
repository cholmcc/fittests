from scipy.optimize import curve_fit
from scipy.stats    import chi2 as chisquare
from numpy          import genfromtxt, histogram, sqrt, exp, linspace, sum, pi

def f(x,mu,sigma):
    return 1/(sqrt(2*pi)*sigma)*exp(-((x-mu)/sigma)**2/2)

def g(f,p,x,y,e):
    return sum(((y-f(x,*p))/e)**2)

def fit(filename,show):
    s   = genfromtxt(filename)
    b   = linspace(-3,3,31)
    x   = (b[1:]+b[:-1])/2
    y,_ = histogram(s, b, density=True)
    e   = sqrt(y/len(s)/(b[1:]-b[:-1]))
    xz  = x[e>0]
    yz  = y[e>0]
    ez  = e[e>0]
    
    p,c  = curve_fit(f, xz, yz, (0,1), ez, absolute_sigma=True)
    nu   = len(yz) - len(p)
    chi2 = g(f,p,xz,yz,ez)
    pval = chisquare.sf(chi2,nu)
    
    print('chi^2/nu: {:.1f}/{:d}={:.3f} ({:.1f}%)'
          .format(chi2,nu,chi2/nu,100*pval))
    for pn,pv,pe in zip(['mu','sigma'],p, sqrt(c.diagonal())):
        print('{:6s}: {:5.2f} +/- {:5.2f}'.format(pn,pv,pe))


    if not show:
        return

    import matplotlib.pyplot as plt

    plt.ion()
    plt.errorbar(x,y,e,label=r'$\mathrm{d}N/\mathrm{d}x$')
    plt.plot(x,f(x,*p),label=r'$f(x)$')
    plt.xlabel(r'$x$')
    plt.legend()
    plt.show()
    
if __name__ == '__main__':
    import sys

    fit(sys.argv[1], len(sys.argv) > 2)

    
